package rit.ritcalculosrescisorios;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import java.text.ParseException;
import java.util.Locale;


public class TelaFgtsMulta extends AppCompatActivity {
    Button btnCalcFgtsMulta;
    private Double doubSalario;
    private String strSaldoFgts;
    private String strMulta;
    private String tipoDemissao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_fgts_multa);
        btnCalcFgtsMulta = (Button)findViewById(R.id.btnCalcFgtsMulta);
        final EditText dataAdmissao = (EditText) findViewById(R.id.dataadmissao);
        final EditText dataDemissao = (EditText)findViewById(R.id.datademissao);
        final EditText editTextSalario = (EditText)findViewById(R.id.editTextSalario);
        final RadioButton radioButtonDispensaSJCausa = (RadioButton)findViewById(R.id.radioButton);
        final RadioButton radioButtonTContrato = (RadioButton)findViewById(R.id.radioButtonTContrato);
        final RadioButton radioButtonPedidoDemissao = (RadioButton)findViewById(R.id.radioButtonPedidoDemissao);

        CalcMultaFgts multaFgts = new CalcMultaFgts(); // Cria instancia da classe CalcMultasFgta.


        btnCalcFgtsMulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calcFgtsMulta = new Intent(TelaFgtsMulta.this, TelaResultadoFGTS.class); // Passando valores para classe resultado.
                CalcFgts calcFgts = new CalcFgts();//criando instancia da classe CalcFgts.
                CalcMultaFgts multaFgts = new CalcMultaFgts(); // Cria instancia da classe CalcMultasFgta.
                doubSalario = Double.parseDouble(editTextSalario.getText().toString());//converte o salario de String para double.
                calcFgts.setSalario(doubSalario);//passa o valor do salario em double para classe CalcFgts.

                try {
                    calcFgts.setAdmissao(dataAdmissao.getText().toString());

                    calcFgts.setDemissao(dataDemissao.getText().toString());//passa data Demissao xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.


                    calcFgts.getFgts();
                    // calcFgts.getMeses();
                    calcFgts.getDiasTrab();

                    strSaldoFgts = Double.toString(calcFgts.getFgts()); // retorna o valor do saldo de FGTS em double convertendo em string.

                    multaFgts.setSaldo(calcFgts.getFgts()); // seta o valor do saldo de FGTS para a classe Multa.
                    strMulta = Double.toString(multaFgts.getMulta());// retorna valor da multa da classe passando de double para String.

                    multaFgts.getMulta();
                    double doubMultaMaisFgts = (calcFgts.getFgts() + multaFgts.getMulta());
                    doubMultaMaisFgts = Double.valueOf(String.format(Locale.US, "%.2f", doubMultaMaisFgts));
                    String multaMaisFgts = Double.toString(doubMultaMaisFgts);

                    if(radioButtonDispensaSJCausa.isChecked()){

                        tipoDemissao=(" R$ " + (multaMaisFgts));
                        calcFgtsMulta.putExtra("multa40TelaFgts", strMulta);
                    }
                    if(radioButtonTContrato.isChecked()){
                        tipoDemissao=(" R$ " + strSaldoFgts);
                    }
                    if (radioButtonPedidoDemissao.isChecked()) {
                        tipoDemissao = (" R$ " + strSaldoFgts + "  Obs:  O Valor ficará retido em sua conta do FGTS.");
                    }


                    calcFgtsMulta.putExtra("saldoFgtsTelaFgts", strSaldoFgts);
                    calcFgtsMulta.putExtra("tipoDemissaoTelaFgts", tipoDemissao);

                    startActivity(calcFgtsMulta);

                } catch (ParseException e) {
                    e.printStackTrace();

                }


            }
        });

    }

}
