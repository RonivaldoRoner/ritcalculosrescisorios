package rit.ritcalculosrescisorios;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaFgts extends AppCompatActivity {
    private Button btnCalcFgtsMulta;
    private Button btnMulta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_fgts);

        btnCalcFgtsMulta = (Button)findViewById(R.id.btnCalcFgtsMulta);
        btnMulta = (Button)findViewById(R.id.btnMulta);

        btnCalcFgtsMulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent telaFgtsMulta = new Intent(TelaFgts.this, TelaFgtsMulta.class);
                startActivity(telaFgtsMulta);
            }
        });

        btnMulta.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent telaMulta = new Intent(TelaFgts.this, TelaMulta.class);
                startActivity(telaMulta);
            }
        });


    }
}
