package rit.ritcalculosrescisorios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ronivaldo on 09/06/2016.
 */
public class Rescisao {

    private Date admissao;
    private Date demissao;
    private double tempTrab; //Tempo trabalhado.
    private double salario;
    private double salAdicio;  //Salario mais os adicionais.
    private double adicionais;
    private int tipoRescisao;
    private int tipoAviso;
    private int faltasMes;
    private int faltasAno;
    private int feriasGozadas;
    //private SimpleDateFormat sdfDataHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
    //private SimpleDateFormat sdfData1 = new SimpleDateFormat("dd/MM/yy");
    private double feriasPropor;
    private double feriasVencidas;
    private double decimoTerceiro;


    public Rescisao() {
    }

    public Date getAdmissao(){
        return admissao;
    }

    //formata a data em dd/MM/yyyy
    public String getAdmissaoEmString(){
        return sdfData.format(admissao);
    }

    public void setAdmissao(Date admissao) {
        this.admissao = admissao;
    }

    public void setAdmissao(String admissao) throws ParseException {
        this.admissao = Uteis.converteEFormataData(admissao);
    }


    public Date getDemissao(){

        return demissao;
    }

    public void setDemissao(Date demissao) {
        this.demissao = demissao;
    }

    //transforma Demissao em String e formata para o padrão dd/MM/yyyy
    public String getDemissaoEmString(){
        return sdfData.format(demissao);
    }

    public void setDemissao(String demissao) throws ParseException {
        this.demissao = Uteis.converteEFormataData(demissao);
    }

    //Tempo trabalhado em dias.
    public double getTempTrab(){

        return Uteis.RetornaDiferencaDeDiasEntreDatas(demissao, admissao);

    }

    public void setTempTrab(int tempTrab) {
        this.tempTrab = tempTrab;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getAdicionais() {
        return adicionais;
    }

    public void setAdicionais(double adicionais) {
        this.adicionais = adicionais;
    }

    public double getSalAdicio() {
        return salAdicio=(salario + adicionais);
    }

    public void setSalAdicio(double salAdicio) {
        this.salAdicio = salAdicio;
    }

    public int getTipoRescisao() {
        return tipoRescisao;
    }

    public void setTipoRescisao(int tipoRescisao) {
        this.tipoRescisao = tipoRescisao;
    }

    public int getTipoAviso() {
        return tipoAviso;
    }

    public void setTipoAviso(int tipoAviso) {
        this.tipoAviso = tipoAviso;
    }

    public int getFaltasMes() {
        return faltasMes;
    }

    public void setFaltasMes(int faltasMes) {
        this.faltasMes = faltasMes;
    }

    public int getFaltasAno() {
        return faltasAno;
    }

    public void setFaltasAno(int faltasAno) {
        this.faltasAno = faltasAno;
    }

    public int getFeriasGozadas() {
        return feriasGozadas;
    }

    public void setFeriasGozadas(int feriasGozadas) {
        this.feriasGozadas = feriasGozadas;
    }


}
