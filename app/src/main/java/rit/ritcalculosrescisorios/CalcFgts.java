package rit.ritcalculosrescisorios;

import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ronivaldo on 09/06/2016.
 */
public class CalcFgts {

    private double meses;
    private double salario;
    private double fgts;
    private final double INDIC = 0.08;
    private Date admissao;
    private Date demissao;
    private double diasTrab;
   // private SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");

    public CalcFgts() {
    }

    public CalcFgts(double meses, double salario, double fgts, Date admissao, Date demissao) {
        this.meses = meses;
        this.salario = salario;
        this.fgts = fgts;
        this.admissao = admissao;
        this.demissao = demissao;
    }

    public Date getAdmissao() {

        return admissao;
    }

    public void setAdmissao(Date admissao) {
        this.admissao = admissao;
    }

    public void setAdmissao(String admissao) throws ParseException {

        this.admissao = Uteis.converteEFormataData(admissao);
    }

    public Date getDemissao() {

        return demissao;

    }

    public void setDemissao(Date demissao) {
        this.demissao = demissao;
    }

    public void setDemissao(String demissao) throws ParseException {

        this.demissao = Uteis.converteEFormataData(demissao);
    }

    public double getMeses(){

        return  meses = Uteis.RetornaMesesTrabalhados(demissao, admissao);

    }

    public double getDiasTrab() {
        return diasTrab = Uteis.RetornaDiferencaDeDiasEntreDatas(demissao, admissao);
    }

    public void setDiasTrab(double diasTrab) {
        this.diasTrab = diasTrab;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getFgts() { //Calcula o e retorna o valor de FGTS.

        fgts = ((salario / 30) * INDIC )* diasTrab;

        return fgts = Double.valueOf(String.format(Locale.US, "%.2f", fgts));
    }

    public void setFgts(double fgts) {
        this.fgts = fgts;
    }
}
