package rit.ritcalculosrescisorios;

import android.content.Intent;

import java.util.Locale;

/**
 * Created by Ronivaldo on 09/06/2016.
 */
public class CalcMultaFgts {

    private double multa;
    final private double IND = 0.4;
    private double saldo=0;


    public CalcMultaFgts() {
    }

    public CalcMultaFgts(double multa, double saldo) {
        this.multa = multa;
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {

        this.saldo = saldo;
    }

    public double getMulta() { //Aqui é feito o Calculo da Multa de 40% FGTS.
        multa = (saldo * IND);
        return multa = Double.valueOf(String.format(Locale.US,"%.2f",multa));
    }

    public void setMulta(double multa) {
        this.multa = multa;
    }

}


