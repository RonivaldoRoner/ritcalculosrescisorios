package rit.ritcalculosrescisorios;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class TelaMulta extends AppCompatActivity {
    private double saldoFgts;
    private String tipoDemissao;
    private TextView editTextSalario;
    private Button btnCalcMulta;
    private double doubResMulta;
    private String strResMulta;
    private RadioButton demissao=null, termContrato=null, pedidoDemissao=null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_multa);

        demissao = (RadioButton)findViewById(R.id.radioButtonDSJCausa);
        termContrato = (RadioButton)findViewById(R.id.radioButtonTContrato);
        pedidoDemissao = (RadioButton)findViewById(R.id.radioButtonPedidoDemissao);
        editTextSalario = (TextView) findViewById(R.id.editTextSalario); //inicializo as variavies dizendo que ela é a variavel do layout
        btnCalcMulta = (Button) findViewById(R.id.btnCalcMulta); //com o R.id>editBLABLABLA (isso é o endereço do campo no layout, o ID dele)

        btnCalcMulta.setOnClickListener(new View.OnClickListener() { //seto o click do botão
            @Override
            public void onClick(View v) {


                try{

                    ParseDouble(); // no click do botão chamo esse metodo
                    Intent calc = new Intent(TelaMulta.this, TelaResultadoFGTS.class);
                    CalcMultaFgts saldoCalcMulta = new CalcMultaFgts(); //Chamando função calculamultafgts classe não é activity.
                    saldoCalcMulta.setSaldo(saldoFgts); //passando parametro double para a função calcular a mulda.
                    doubResMulta = saldoCalcMulta.getMulta(); //retornando o valor da multa em double
                    strResMulta = Double.toString(doubResMulta);//Passando de double para String.
                    if(demissao.isChecked()){
                        double somaMultaFgts = saldoFgts + doubResMulta;
                        somaMultaFgts = Double.valueOf(String.format(Locale.US, "%.2f", somaMultaFgts));
                        tipoDemissao=("R$ " + (somaMultaFgts));
                        calc.putExtra("multa40", strResMulta);
                    }
                    if(termContrato.isChecked()){
                        tipoDemissao=("R$ " + saldoFgts);
                    }
                    if (pedidoDemissao.isChecked()) {
                        tipoDemissao = (" R$" + saldoFgts + "   OBS: O Valor ficará retido em sua conta do FGTS.");
                    }
                    calc.putExtra("saldoFgts", editTextSalario.getText().toString());
                    calc.putExtra("tipoDemissao", tipoDemissao);
                    startActivity(calc);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

// Toast.makeText(this, "Pedido de demissão." + saldoFgts, Toast.LENGTH_SHORT).show();
    private void ParseDouble()
    {
        try {
            saldoFgts = Double.parseDouble(editTextSalario.getText().toString()); // tento pegar o valor digitado no editext e converter pra double

            ///Toast.makeText(this, "O saldo é: " + saldoFgts, Toast.LENGTH_SHORT).show();//exibo um alert com o resultado na tela
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Erro ao converter o saldo digitado" , Toast.LENGTH_SHORT).show();//se houver erro pra converter o valor digitado em double
                                                                                                    //exibe essa msg faland q deu erro
        }


    }


}

