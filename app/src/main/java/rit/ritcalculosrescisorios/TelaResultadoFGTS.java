package rit.ritcalculosrescisorios;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class TelaResultadoFGTS extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_resultado_fgts);

        try{

            Intent telaMultaEFgts = getIntent();

            String saldo;
            String stringMulta;
            String tipo;

            saldo = (String)telaMultaEFgts.getSerializableExtra("saldoFgts");
            if(saldo == "0" || saldo == null){
                saldo = null;
                saldo = (String)telaMultaEFgts.getSerializableExtra("saldoFgtsTelaFgts");
            }
            TextView textViewSaldoFgts = (TextView)findViewById(R.id.textViewSaldoFgts);
            textViewSaldoFgts.setText(saldo);

            stringMulta = (String)telaMultaEFgts.getSerializableExtra("multa40");
            if(stringMulta == "0" || stringMulta == null){
                stringMulta = null;
                stringMulta = (String)telaMultaEFgts.getSerializableExtra("multa40TelaFgts");
            }
            TextView textViewMulta40 = (TextView)findViewById(R.id.textViewMulta40);
            textViewMulta40.setText(stringMulta);

            tipo = (String)telaMultaEFgts.getSerializableExtra("tipoDemissao");
            if(tipo == "0" || tipo == null){
                tipo = null;
                tipo = (String)telaMultaEFgts.getSerializableExtra("tipoDemissaoTelaFgts");
            }
            TextView textViewTipoRescisao = (TextView)findViewById(R.id.textViewTipoRescisao);
            textViewTipoRescisao.setText(tipo);


        }catch (Exception e){
            e.printStackTrace();
        }


    }

}
