package rit.ritcalculosrescisorios;

import android.app.DatePickerDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Ronivaldo on 09/06/2016.
 */




public class Uteis {
    //Conversão de dia em milisegundos.
    private static long timeSegundos = 86400000;

    public static double RetornaDiferencaDeDiasEntreDatas(Date paramData1, Date paramData2)
    {
        long diff = Math.abs(paramData1.getTime() - paramData2.getTime());
        return  (diff / timeSegundos);
    }

    public static double RetornaMesesTrabalhados(Date paramData1, Date paramData2)
    {

        long diff = Math.abs(paramData1.getTime() - paramData2.getTime());

        double numeroDeMeses = ((diff / timeSegundos)/30);
        numeroDeMeses = Double.valueOf(String.format(Locale.US, "%.1f", numeroDeMeses));

        return (numeroDeMeses);
    }

    public static boolean VerificarAnoBissexto(int paramAno)
    {
        boolean ehAnoBissexto = false;
        // se o resto da divisiao do ano por 400 for = 0 é ano bissexto.
        if(paramAno % 400 == 0){
            ehAnoBissexto = true;
            // se o resto da divisiao do ano por 4 for = 0,
            // e o resto da divisão do ano for diferente de 0,
            // é ano bissexto.
        } else if((paramAno % 4 == 0) && (paramAno % 100 != 0)){
            ehAnoBissexto = true;
            //Se não ele não é um ano bissexto.
        } else{
            ehAnoBissexto = false;
        }

        return ehAnoBissexto;
    }

    public static Date converteEFormataData(String paramData) throws ParseException {

        SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfData1 = new SimpleDateFormat("dd/MM/yy");
        Date dataRetorno = null;


        try {
            dataRetorno = sdfData.parse(paramData);
        } catch (ParseException e) {
            if (paramData.length() == 10) {
                try {
                    dataRetorno = sdfData1.parse(paramData);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();

            System.out.println("Você informou uma data no formato inválido.");

        }
        return dataRetorno;

    }




}



