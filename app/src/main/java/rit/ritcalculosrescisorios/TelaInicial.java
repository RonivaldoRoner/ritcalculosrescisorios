package rit.ritcalculosrescisorios;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

public class TelaInicial extends AppCompatActivity {

    private Button btnOpFgtsMulta;
    private Button btnOpRescisao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);

        btnOpFgtsMulta = (Button)findViewById(R.id.btnOpFgtsMulta);
        btnOpRescisao = (Button)findViewById(R.id.btnOpRescisao);

        btnOpFgtsMulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent telaFgts = new Intent(TelaInicial.this, TelaFgts.class);
                startActivity(telaFgts);
            }
        });

        btnOpRescisao.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent telaRescisao = new Intent(TelaInicial.this, TelaRescisao.class);
                startActivity(telaRescisao);
            }
        });
    }
}
